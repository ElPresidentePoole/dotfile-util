import config
from shutil import copyfile
from os.path import isdir
from os import makedirs
from os.path import isfile
from pathlib import Path

def swap_tilde_for_home(path):
    """ input: string containing ~ (e.g. ~/.config/i3wm/config)
        output: string containing path (e.g. /home/foobar/i3wm/config)
    """
    return path.replace('~', str(Path.home()))

def list_files_being_replaced():
    """ output: 
    """
    replaced_files = ''
    for fp in config.DOTFILES:
        replaced_files += fp + '\n'
    replaced_files += '\n'
    return replaced_files

def main():
    if input('\n' + list_files_being_replaced() + 'WARNING: THE ABOVE FILES WILL BE REPLACED.  DO YOU WISH TO CONTINUE Y/N?\n').lower() != 'y':
        return
    git_folder = swap_tilde_for_home(config.GIT_FOLDER)

    if isdir(git_folder):
        print('Copying dotfiles to home...')
        for file_path in config.DOTFILES:
            file_in_backup = file_path.replace('~/', git_folder)
            if isfile(file_in_backup):  # get file in git folder
                print('Copying', file_in_backup, 'to ~')
                dest_dir = file_path.replace('~/', swap_tilde_for_home('~/'))
                dest_dir_parent = '/'.join(dest_dir.split('/')[:-1])

                if not isdir(dest_dir_parent):
                    makedirs(dest_dir_parent)
                print(dest_dir)
                copyfile(file_in_backup, dest_dir)
            else:
                print('Error: file', file_path, 'not found, skipping...')
    else:
        print(config.GIT_FOLDER, 'is not a valid directory.')

if __name__ == '__main__':
    main()
