"""
    Example configs:
    DOTFILES = ['~/.Xresources',
                '~/.bashrc']
    
    GIT_FOLDER = '~/examplefolder/'
"""

DOTFILES = []
GIT_FOLDER = '~/git_folder'