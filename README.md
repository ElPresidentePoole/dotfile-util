# dotfile-util

A program I made to make backing up and restoring dotfiles from git easier.

To use this program, simply edit config.py to point to the dotfiles you wish to backup, and the git folder you use to manage your dotfiles.

Then you can run dotfile-backup.py to copy your dotfiles to your git folder, and run dotfile-restore.py to copy your dotfiles to your home folder.  **Running dotfile-restore.py will overwrite your current dotfiles, so be careful!**

This program is licensed under the GNU General Public License.  See LICENSE.txt for more information.