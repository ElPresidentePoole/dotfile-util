import config
from shutil import copyfile
from shutil import rmtree
from os.path import isdir
from os.path import isfile
from os.path import join
from os import makedirs
from pathlib import Path

def swap_tilde_for_home(path):
    """ input: string containing ~ (e.g. ~/.config/i3wm/config)
        output: string containing path (e.g. /home/foobar/i3wm/config)
    """
    return path.replace('~', str(Path.home()))

def main():
    git_folder = swap_tilde_for_home(config.GIT_FOLDER)

    if isdir(git_folder):
        print('Copying specified dotfiles to', git_folder + '...')
        for file_path in config.DOTFILES:
            if isfile(swap_tilde_for_home(file_path)):
                # dest_dir is the destination for the file, including the filename
                dest_dir = file_path.replace('~/', git_folder)
                
                # The below does the following in order:
                # 1. splits up the dest_dir by / (folders)
                # 2. removes the last folder to get us the parent folder of the dest_dir
                # 3. joins the list back together so we get the parent folder as a string
                dest_dir_parent = '/'.join(dest_dir.split('/')[:-1])

                # deletes the folder and recreates it
                # basically wipes all unused dot files from the folder
                # commented out because 1. it can cause problems on network drives
                # and 2. will cause problems with git
                # just delete unused dotfiles manually
                # rmtree('/home/gordon/testfolder/')
                print('Copying', file_path, 'to', git_folder)

                if not isdir(dest_dir_parent):
                    makedirs(dest_dir_parent)
                copyfile(swap_tilde_for_home(file_path), dest_dir)
            else:
                print('Error: file', file_path, 'not found, skipping...')
    else:
        print(config.GIT_FOLDER, 'is not a valid directory.')

if __name__ == '__main__':
    main()